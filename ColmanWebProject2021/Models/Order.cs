﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace ColmanWebProject2021.Models
{
    public class Order
    {
        public int Id { get; set; }
        
        [Display(Name = "Customer")]
        public User Customer { get; set; }

        public string CustomerId { get; set; }
        
        public Restaurant Restaurant { get; set; }

        public int RestaurantId { get; set; }
        
        [Display(Name = "Date And Time")]
        public DateTime DateTime { get; set; }

        public string Comments { get; set; }
        
        [Display(Name = "Number Of Seats")]
        public int NumberOfSeats { get; set; }
    }
}
