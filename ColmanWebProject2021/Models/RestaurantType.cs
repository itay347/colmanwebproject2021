﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ColmanWebProject2021.Data;

namespace ColmanWebProject2021.Models
{
    public class RestaurantType : IValidatableObject
    {
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 1)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(200, MinimumLength = 1)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var context =
                (ColmanWebProject2021Context) validationContext.GetService(typeof(ColmanWebProject2021Context));
            
            var existingRestaurantType = context.RestaurantType.FirstOrDefault(rt => rt.Name.ToLower().Equals(Name.ToLower()));

            if (existingRestaurantType != null)
            {
                yield return new ValidationResult(
                    "A restaurant type with the same name already exists.",
                    new[] { nameof(Name) });
            }
        }
    }
}
