﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ColmanWebProject2021.Data;

namespace ColmanWebProject2021.Models.CustomValidationAttributes
{
    public class UniqueRestaurantNameAttribute : ValidationAttribute
    {
        public UniqueRestaurantNameAttribute()
        {
        }

        public string GetErrorMessage() =>
            "A restaurant with the same name already exists.";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var name = ((String)value).ToLower();

            var context =
                (ColmanWebProject2021Context)validationContext.GetService(typeof(ColmanWebProject2021Context));
            
            var existingRestaurant = context.Restaurant.FirstOrDefault(r => r.Name.ToLower().Equals(name));

            if (existingRestaurant != null)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}
