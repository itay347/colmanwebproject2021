﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ColmanWebProject2021.Data;
using ColmanWebProject2021.Models.CustomValidationAttributes;

namespace ColmanWebProject2021.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(30, MinimumLength = 1)]
        [UniqueRestaurantName]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [StringLength(200, MinimumLength = 1)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Type")]
        public RestaurantType Type { get; set; }

        [Range(1, 1000)]
        [Required]
        [Display(Name = "Guest Capacity")]
        public int GuestCapacity { get; set; }

        [Display(Name = "Accessible")]
        public bool Accessible { get; set; }

        [Display(Name = "Kosher")]
        public bool Kosher { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:hh\\:mm tt}")]
        [DataType(DataType.Time)]
        [Required]
        [Display(Name = "Opening time")]
        public DateTime OpeningTime { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:hh\\:mm tt}")]
        [DataType(DataType.Time)]
        [Required]
        [Display(Name = "Closing time")]
        public DateTime ClosingTime { get; set; }

        [Display(Name = "Image url")]
        public string ImgUrl { get; set; }
    }
}
