using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanWebProject2021.Data;
using ColmanWebProject2021.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace ColmanWebProject2021.Controllers
{
    public class OrdersController : Controller
    {
        private readonly ColmanWebProject2021Context _context;
        private readonly UserManager<User> _userManager;

        public OrdersController(ColmanWebProject2021Context context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Orders
        [Authorize]
        public async Task<IActionResult> Index(string restaurantName, DateTime fromDate, DateTime toDate)
        {
            IQueryable<Order> orders = _context.Order.Include(o => o.Restaurant);

            // Build the where clause according to the filter parameters:
            // Check is the restaurantName filter was filled
            if (!String.IsNullOrEmpty(restaurantName))
            {
                orders = orders.Where(o => o.Restaurant.Name.Contains(restaurantName));
            }

            // Check if the fromDate filter was filled
            if (fromDate != DateTime.MinValue)
            {
                orders = orders.Where(o => o.DateTime.Date >= fromDate.Date);
            }

            // Check if the toDate filter was filled
            if (toDate != DateTime.MinValue)
            {
                orders = orders.Where(o => o.DateTime.Date <= toDate);
            }

            // Check if the user isn't an admin.
            if (!User.IsInRole("Admin"))
            {

                // Get the connected user.
                User user = await _userManager.GetUserAsync(HttpContext.User);

                // Get only orders that belong to the connected user.
                orders = orders.Where(o => o.CustomerId == user.Id);
            }

            List<Order> list = await orders.ToListAsync();

            return View(list);
        }

        public async Task<IActionResult> Search(string restaurantName, DateTime fromDate, DateTime toDate)
        {
            IQueryable<Order> orders = _context.Order.Include(o => o.Restaurant);

            // Build the where clause according to the filter parameters:
            // Check is the restaurantName filter was filled
            if (!String.IsNullOrEmpty(restaurantName))
            {
                orders = orders.Where(o => o.Restaurant.Name.Contains(restaurantName));
            }

            // Check if the fromDate filter was filled
            if (fromDate != DateTime.MinValue)
            {
                orders = orders.Where(o => o.DateTime.Date >= fromDate.Date);
            }

            // Check if the toDate filter was filled
            if (toDate != DateTime.MinValue)
            {
                orders = orders.Where(o => o.DateTime.Date <= toDate);
            }

            // Check if the user isn't an admin.
            if (!User.IsInRole("Admin"))
            {

                // Get the connected user.
                User user = await _userManager.GetUserAsync(HttpContext.User);

                // Get only order that belong to the connected user.
                orders = orders.Where(o => o.CustomerId == user.Id);
            }

            List<Order> list = orders.ToList();

            return PartialView("List", list);
        }

        // GET: Orders/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            var order = await _context.Order
                .Include(o => o.Customer)
                .Include(o => o.Restaurant)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (order == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            // Get the connected user.
            User user = await _userManager.GetUserAsync(HttpContext.User);

            if (!User.IsInRole("Admin") && !user.Id.Equals(order.CustomerId))
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            return View(order);
        }

        // GET: Orders/Create
        [Authorize]
        public async Task<IActionResult> Create(string? restaurantId)
        {
            Restaurant restaurant = null;

            // Check if the id field is filled and valid, and if so, hide the restaurant field.
            if (!string.IsNullOrEmpty(restaurantId))
            {
                TempData["restaurantId"] = restaurantId;
                int id = Int32.Parse(restaurantId);
                restaurant = await _context.Restaurant.FirstOrDefaultAsync(r => r.Id == id);

                if (restaurant != null)
                {
                    ViewData["TitleAddition"] = "for " + restaurant.Name;
                }
            }

            ViewBag.showRestaurants = (restaurant == null);
            TempData["showRestaurants"] = ViewBag.showRestaurants;
            
            PopulateRestaurantsDropDownList();
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,RestaurantId,DateTime,Comments,NumberOfSeats")] Order order)
        {
            if (ModelState.IsValid)
            {
                // Get the connected user.
                User user = await _userManager.GetUserAsync(HttpContext.User);
                order.Customer = user;

                // If the restaurant was already selected beforehand - add it here.
                if (!(bool)TempData["showRestaurants"])
                    order.RestaurantId = Int32.Parse(TempData["restaurantId"] as string);

                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Orders/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }
            
            var order = await _context.Order.Include(o => o.Customer).FirstOrDefaultAsync(m => m.Id == id);

            if (order == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            // Get the connected user.
            User user = await _userManager.GetUserAsync(HttpContext.User);

            if (!User.IsInRole("Admin") && !user.Id.Equals(order.CustomerId))
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            PopulateRestaurantsDropDownList();
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,RestaurantId,DateTime,Comments,NumberOfSeats")] Order order)
        {
            if (id != order.Id)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    order.Customer = await _userManager.GetUserAsync(HttpContext.User);
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return new NotFoundViewResult("NotFoundError", "Order was not found");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            var order = await _context.Order
                .Include(o => o.Restaurant)
                .Include(o => o.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (order == null)
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            // Get the connected user.
            User user = await _userManager.GetUserAsync(HttpContext.User);

            if (!User.IsInRole("Admin") && !user.Id.Equals(order.CustomerId))
            {
                return new NotFoundViewResult("NotFoundError", "Order was not found");
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.Id == id);
        }

        private void PopulateRestaurantsDropDownList(object selectedRestaurants = null)
        {
            var restaurantQuery =
                from r in _context.Restaurant
                select r;

            ViewBag.RestaurantId = new SelectList(restaurantQuery, "Id", "Name", selectedRestaurants);
        }
    }
}
