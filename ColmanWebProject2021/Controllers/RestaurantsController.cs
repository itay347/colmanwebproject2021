﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ColmanWebProject2021.Data;
using ColmanWebProject2021.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using ColmanWebProject2021.Handlers;

namespace ColmanWebProject2021.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly ColmanWebProject2021Context _context;

        public RestaurantsController(ColmanWebProject2021Context context)
        {
            _context = context;
        }

        // GET: Restaurants
        public async Task<IActionResult> Index()
        {
            if (String.Compare(HttpContext.Session.GetString("navigatedFrom"), "RestaurantType") == 0)
            {
                HttpContext.Session.SetString("navigatedFrom", "");
                ViewBag.typeName = HttpContext.Session.GetString("restaurantTypeName");
            }
            else HttpContext.Session.SetString("restaurantTypeName", "");

            return View();
        }

        // GET: Restaurants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant wasn't found");
            }

            var restaurant = await _context.Restaurant.Include("Type").SingleOrDefaultAsync(r => r.Id == id);

            if (restaurant == null)
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant wasn't found");
            }

            return View(restaurant);
        }

        // GET: Restaurants/Admin
        [Authorize(Roles = "Admin")]
        public IActionResult Admin()
        {
            return View();
        }

        // GET: Restaurants/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Restaurants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Type,Address,Description,GuestCapacity,Accessible,Kosher,OpeningTime,ClosingTime,ImgUrl")] Restaurant restaurant)
        {
            var type = _context.RestaurantType.Where(t => t.Name == restaurant.Type.Name);

            if (type.Any())
            {
                restaurant.Type = type.ToList()[0];
                ModelState.Remove("Type.Name");
                ModelState.Remove("Type.Description");
                if (ModelState.IsValid)
                { 
                    _context.Add(restaurant);
                    await _context.SaveChangesAsync();
                    try
                    {
                        FaceBookHandler.PostMessage(restaurant);
                    }
                    catch (Exception e)
                    {
                        await Console.Error.WriteLineAsync(e.Message);
                    }
                    return RedirectToAction(nameof(Index));
                }

                return View(restaurant);
            }
            else
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant type wasn't found");
            }
        }

        // GET: Restaurants/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant wasn't found");
            }

            var restaurant = _context.Restaurant.Include(r => r.Type).Where(r => r.Id == id);
            if (!restaurant.Any())
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant wasn't found");
            }

            return View(restaurant.ToList()[0]);
        }

        // POST: Restaurants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Type,Address,Description,GuestCapacity,Accessible,Kosher,OpeningTime,ClosingTime,ImgUrl")] Restaurant restaurant)
        {
            if (id != restaurant.Id)
            {
                return new NotFoundViewResult("NotFoundError", "Restaurant wasn't found");
            }

            ModelState.Remove("Type.Name");
            ModelState.Remove("Type.Description");
            if (ModelState.IsValid)
            {
                try
                {
                    var type = _context.RestaurantType.Where(t => t.Name == restaurant.Type.Name);
                    if (type.Any())
                    {
                        restaurant.Type = type.ToList()[0];
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return new NotFoundViewResult("NotFoundError", "Restaurant type wasn't found");
                    }

                    _context.Update(restaurant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantExists(restaurant.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(restaurant);
        }

        // GET: Restaurants/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurant
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // POST: Restaurants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _context.Restaurant.FindAsync(id);
            _context.Restaurant.Remove(restaurant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurant.Any(e => e.Id == id);
        }

        public IActionResult GetRestaurantTypes()
        {
            var types = (from type in _context.RestaurantType
                         select new { type.Name }).Distinct();

            return Json(types.ToList());
        }

        public IActionResult Search(string restaurantName, string typeName, string accessibility)
        {
            ViewBag.typeName = "";
            var restaurants = from restaurant in _context.Restaurant
                       select restaurant;
            restaurants = restaurants.OrderBy(r => r.Name);

            if (!String.IsNullOrEmpty(restaurantName))
            {
                restaurants = restaurants.Where(r => r.Name.ToUpper().Contains(restaurantName.ToUpper()));
            }

            if (!String.IsNullOrEmpty(typeName))
            {
                restaurants = restaurants.Where(r => r.Type.Name.Equals(typeName));
            }

            if (!String.IsNullOrEmpty(accessibility))
            {
                bool isAccessible = accessibility.ToLower().Equals("true");
                restaurants = restaurants.Where(r => r.Accessible.Equals(isAccessible));
            }

            if (restaurants.Any())
            {
                return PartialView("List", restaurants.Include(r => r.Type).ToList());
            }
            else
            {
                return PartialView("List", new List<Restaurant>());
            }
        }

        [Authorize(Roles = "Admin")]
        public JsonResult MostPopularRestaurants()
        {
            var result = (
                from order in _context.Order
                join restaurant in _context.Restaurant
                    on order.RestaurantId equals restaurant.Id
                group order by restaurant.Name into g
                select new
                {
                    restaurantName = g.Key,
                    orders = g.Count()
                } into x
                orderby x.orders descending
                select x
            ).Take(5).ToList();

            return Json(result);
        }

        [Authorize(Roles = "Admin")]
        public JsonResult AvrageNumberOfSeatsPerRestaurantInMonth(int month)
        {
            if (month < 1 || month > 12)
            {
                return Json(null);
            }
            else
            {
                var result = this._context.Order.Include(o=>o.Restaurant).AsEnumerable().Where(r => r.DateTime.Year == DateTime.Now.Year && r.DateTime.Month == month)
                    .GroupBy(x => x.Restaurant.Name).Select(s =>
              new { name = s.Key, value = s.Sum(s=> s.NumberOfSeats) / s.ToList().Count }).ToList();
                return Json(result);
            }
        }
    }
}
