﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanWebProject2021.Data;
using ColmanWebProject2021.Models;
using Microsoft.AspNetCore.Http;

namespace ColmanWebProject2021.Controllers
{
    public class RestaurantTypesController : Controller
    {
        private readonly ColmanWebProject2021Context _context;

        public RestaurantTypesController(ColmanWebProject2021Context context)
        {
            _context = context;
        }

        // GET: RestaurantTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.RestaurantType.ToListAsync());
        }

        // GET: RestaurantTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @restaurantType = await _context.RestaurantType
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@restaurantType == null)
            {
                return NotFound();
            }

            HttpContext.Session.SetString("restaurantTypeId", restaurantType.Id.ToString());
            HttpContext.Session.SetString("restaurantTypeName", restaurantType.Name);

            return View(restaurantType);
        }

        // GET: RestaurantTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RestaurantTypes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description")] RestaurantType restaurantType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(restaurantType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(restaurantType);
        }

        // GET: RestaurantTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurantType = await _context.RestaurantType.FindAsync(id);
            if (restaurantType == null)
            {
                return NotFound();
            }
            return View(restaurantType);
        }

        // POST: RestaurantTypes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description")] RestaurantType restaurantType)
        {
            if (id != restaurantType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restaurantType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestaurantTypeExists(restaurantType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(restaurantType);
        }

        // GET: RestaurantTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurantType = await _context.RestaurantType
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurantType == null)
            {
                return NotFound();
            }

            return View(restaurantType);
        }

        // POST: RestaurantTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurantType = await _context.RestaurantType.FindAsync(id);
            _context.RestaurantType.Remove(restaurantType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantTypeExists(int id)
        {
            return _context.RestaurantType.Any(e => e.Id == id);
        }

        public IActionResult GetRestaurantTypesDetails()
        {
            int restaurantTypesId = Int32.Parse(HttpContext.Session.GetString("restaurantTypeId"));

            int numOfRestaurants = (from restaurant in _context.Restaurant
                             where restaurant.Type.Id == restaurantTypesId
                             select restaurant.Type.Id).Distinct().Count();

            int numOfOrders = (from type in _context.RestaurantType
                                join restaurant in _context.Restaurant
                                on type.Id equals restaurant.Type.Id
                                join order in _context.Order
                                on restaurant.Id equals order.Restaurant.Id
                                where type.Id == restaurantTypesId
                               select type.Id).Count();

            return Json(new { numofrestaurants = numOfRestaurants, numoforders= numOfOrders });
        }

        [HttpPost]
        public ActionResult NavToRestaurants()
        {
            HttpContext.Session.SetString("restaurantTypeName", HttpContext.Session.GetString("restaurantTypeName"));
            HttpContext.Session.SetString("navigatedFrom", "RestaurantType");
            return Json(Url.Action("Index", "Restaurants"));
        }

        public IActionResult Search(string restaurantTypeName, string restuarantTypeDescription)
        {
            ViewBag.typeName = "";
            var restaurantTypes = from RestaurantType in _context.RestaurantType
                        select RestaurantType;
            restaurantTypes = restaurantTypes.OrderBy(r => r.Name);

            if (!String.IsNullOrEmpty(restaurantTypeName))
                restaurantTypes = restaurantTypes.Where(r => r.Name.ToUpper().Contains(restaurantTypeName.ToUpper()));

            if (!String.IsNullOrEmpty(restuarantTypeDescription))
                restaurantTypes = restaurantTypes.Where(r => r.Description.ToUpper().Contains(restuarantTypeDescription.ToUpper()));

            if (restaurantTypes.Count() > 0)
                return PartialView("List", restaurantTypes.ToList());
            else return PartialView("List", new List<ColmanWebProject2021.Models.RestaurantType>());
        }



    }
}
