﻿using System;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Net;
using ColmanWebProject2021.Models;
using Newtonsoft.Json;
using Facebook;

namespace ColmanWebProject2021.Handlers
{
    public class FaceBookHandler 
    {
        // https://developers.facebook.com/tools/explorer/373964617688335/?method=POST&path=101225788921346%2Ffeed&version=v11.0&message=hello%20world

        private const string PageId = "101225788921346";                                      
        
        public static void PostMessage(Restaurant restaurant)
        {
            var accessToken = Environment.GetEnvironmentVariable("FACEBOOK_ACCESS_TOKEN");
            FacebookClient facebookClient = new FacebookClient(accessToken);

            dynamic messagePost = new ExpandoObject();
            messagePost.access_token = accessToken;
            messagePost.message =
                                  "Check Out our new restaurant " + restaurant.Name + "! 🍽 \n" +
                                  "Highly recommended for " + restaurant.Type.Name + " lovers ❤\n" +
                                  "Come to visit us in " + restaurant.Address + " 📍";

            string url = string.Format("/{0}/feed", PageId);
            var result = facebookClient.Post(url, messagePost);
        }
    }
}
