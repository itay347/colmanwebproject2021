﻿$(function () {

    // Init restaurant list
    $(document).ready(function () {

        // Restaurant types
        $.ajax({
            type: 'GET',
            url: "/Restaurants/GetRestaurantTypes",
            contentType: 'json',
            success: function (types) {
                let select = document.getElementById("restaurantTypeName");
                for (let i = 0; i < types.length; i++) {
                    let option = document.createElement("option");
                    option.text = types[i].name;
                    option.value = types[i].name;
                    select.add(option);
                }
                let typeinput = document.getElementById("type_name");
                if (typeinput != null && document.getElementById("type_name").value.localeCompare("") != 0)
                    select.value = document.getElementById("type_name").value;

                $.ajax({
                    type: 'GET',
                    url: "/Restaurants/Search",
                    data: $("#searchForm").serialize(),
                    success: function (data) {
                        $("#restaurantsList").html(data);
                    }
                })
            }
        })

    });

    $("#nameForm").keyup(function () {
        $.ajax({
            type: 'GET',
            url: "/Restaurants/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantsList").html(data);
            }
        })
    })

    $("#searchForm").change(function () {
        $.ajax({
            type: 'GET',
            url: "/Restaurants/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantsList").html(data);
            }
        })
    })
});