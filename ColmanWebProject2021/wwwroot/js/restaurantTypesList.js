﻿$(function () {

    // Init restaurant list
    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: "/RestaurantTypes/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantTypesList").html(data);
            }
        })
    });

    // Restaurant list
    $("#searchButton").click(function () {
        $.ajax({
            type: 'GET',
            url: "/RestaurantTypes/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantTypesList").html(data);
            }
        })
    });


    $("#nameForm").keyup(function () {
        $.ajax({
            type: 'GET',
            url: "/RestaurantTypes/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantTypesList").html(data);
            }
        })
    })

    $("#descriptionForm").keyup(function () {
        $.ajax({
            type: 'GET',
            url: "/RestaurantTypes/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantTypesList").html(data);
            }
        })
    })


    $("#searchForm").change(function () {
        $.ajax({
            type: 'GET',
            url: "/RestaurantTypes/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#restaurantTypesList").html(data);
            }
        })
    })
});