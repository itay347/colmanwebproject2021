﻿$(function () {

    $(document).ready(function () {

        var url = "/RestaurantTypes/GetRestaurantTypesDetails"
        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'json',
            success: function (data) {
                document.getElementById('num_of_restaurants').innerHTML = data.numofrestaurants;
                document.getElementById('num_of_orders').innerHTML = data.numoforders;
            }
        })
    });

    $("#restaurantsButton").click(function () {
        $.ajax({
            type: 'POST',
            url: "/RestaurantTypes/NavToRestaurants",
            dataType: 'json',
            crossDomain: true,
            success: function (data) {
                window.location.href = data;
            }
        })
    });
});