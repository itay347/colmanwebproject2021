﻿$(function () {

    // Init the orders list
    $(document).ready(function () {

        $.ajax({
            type: 'GET',
            url: "/Orders/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#ordersList").html(data);
            }
        });
    });

    $("#searchForm").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: "/Orders/Search",
            data: $("#searchForm").serialize(),
            success: function (data) {
                $("#ordersList").html(data);
            }
        });
    });

});