﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ColmanWebProject2021.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ColmanWebProject2021.Data
{
    public class ColmanWebProject2021Context : IdentityDbContext<User>
    {
        public ColmanWebProject2021Context (DbContextOptions<ColmanWebProject2021Context> options)
            : base(options)
        {
        }

        public DbSet<ColmanWebProject2021.Models.RestaurantType> RestaurantType { get; set; }

        public DbSet<ColmanWebProject2021.Models.Restaurant> Restaurant { get; set; }

        public DbSet<ColmanWebProject2021.Models.Order> Order { get; set; }
    }
}
