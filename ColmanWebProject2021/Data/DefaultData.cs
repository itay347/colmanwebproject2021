﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using ColmanWebProject2021.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ColmanWebProject2021.Data
{
    public static class DefaultData
    {
        private const string AdminRoleName = "Admin";

        public static void EnsureDbCreatedAndHasUsersAndData(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                EnsureDbCreatedAndMigrated(serviceProvider);
                AddDefaultUsersAndRoles(serviceProvider).Wait();
                AddDefaultData(serviceProvider);
            }
        }

        private static void EnsureDbCreatedAndMigrated(IServiceProvider serviceProvider)
        {
            try
            {
                var context = serviceProvider.GetRequiredService<ColmanWebProject2021Context>();
                context.Database.Migrate();
            }
            catch (Exception ex)
            {
                var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "An error occurred while creating/migrating the DB");
            }
        }

        private static async Task AddDefaultUsersAndRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();

            // Make sure there is an Admin role
            var adminRoleExists = await roleManager.RoleExistsAsync(AdminRoleName);
            if (!adminRoleExists)
            {
                await roleManager.CreateAsync(new IdentityRole(AdminRoleName));
            }

            var users = new List<string>() {"user"};
            var admins = new List<string>() {"itay", "amir", "linoy"};
            users.AddRange(admins);

            // Add default users
            foreach (var userName in users)
            {
                var user = new User { UserName = userName, Email = $"{userName}@example.com" };
                var result = await userManager.CreateAsync(user, "123456");

                if (result.Succeeded && admins.Contains(userName))
                {
                    // Add admin role to relevant users
                    await userManager.AddToRoleAsync(user, AdminRoleName);
                }
            }
        }

        private static void AddDefaultData(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ColmanWebProject2021Context>();

            var restaurantTypes = new List<RestaurantType>()
            {
                new()
                {
                    Name = "Italian",
                    Description = "Italian restaurants, pastas, pizzas, lasagnas and more..."
                },
                new()
                {
                    Name = "Cafe",
                    Description = "Sandwiches, Soup, Salads, Breakfast, Pastries and Desserts"
                },
                new()
                {
                    Name = "Fast Food",
                    Description = "Burgers, Snacks, Sandwiches, Fried chicken, French fries, Onion rings, Chicken nuggets and more..."
                },
                new()
                {
                    Name = "Asian",
                    Description = "Panipuri, Noodles, Raman, Fried Rice, Sushi and more..."
                },
                new()
                {
                    Name = "Sweets",
                    Description = "Chocolate, ice cream, pancakes, waffles and so..."
                }
            };

            if (!context.RestaurantType.Any())
            {
                context.RestaurantType.AddRange(restaurantTypes);
                context.SaveChanges();
            }

            var restaurants = new List<Restaurant>()
            {
                new()
                {
                    Name = "Italiano",
                    Address = "13 Allenby, Tel Aviv-Yafo",
                    Description = "Pastas, pizzas, lasagnas and more...",
                    Type = restaurantTypes[0],
                    GuestCapacity = 20,
                    Accessible = true,
                    Kosher = false,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("10:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("23:00:00")),
                    ImgUrl = "/Content/italian.jpg"
                },
                new()
                {
                    Name = "Cafe Cafe",
                    Address = "53 Yigal Alon, Tel Aviv-Yafo",
                    Description = "Sandwiches, Soup, Salads, Breakfast, Pastries and Desserts",
                    Type = restaurantTypes[1],
                    GuestCapacity = 25,
                    Accessible = true,
                    Kosher = true,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("07:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("12:00:00")),
                    ImgUrl = "/Content/salad.jpg"
                },
                new()
                {
                    Name = "Fasty",
                    Address = "7 Jabotinski, Tel Aviv-Yafo",
                    Description = "Burgers, Snacks, Sandwiches, Fried chicken, French fries, Onion rings, Chicken nuggets and more...",
                    Type = restaurantTypes[2],
                    GuestCapacity = 15,
                    Accessible = false,
                    Kosher = false,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("12:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("22:00:00")),
                    ImgUrl = "/Content/fast-food.jpg"
                },
                new()
                {
                    Name = "Japanika",
                    Address = "30 Emil Zola, Tel Aviv-Yafo",
                    Description = "Panipuri, Noodles, Raman, Fried Rice, Sushi and more...",
                    Type = restaurantTypes[3],
                    GuestCapacity = 20,
                    Accessible = true,
                    Kosher = false,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("12:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("18:00:00")),
                    ImgUrl = "/Content/japan.jpg"
                },
                new()
                {
                    Name = "Sweet Land",
                    Address = "2/4 Haagshama Ashdod",
                    Description = "Chocolate, ice cream, pancakes, waffles and so...",
                    Type = restaurantTypes[4],
                    GuestCapacity = 10,
                    Accessible = false,
                    Kosher = true,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("10:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("19:00:00")),
                    ImgUrl = "/Content/sweet.jpg"
                },
                new()
                {
                    Name = "Burger Burger",
                    Address = "30 Jabotinski, Tel Aviv-Yafo",
                    Description = "Burgers, Fried chicken, French fries, Onion rings, Chicken nuggets and more...",
                    Type = restaurantTypes[2],
                    GuestCapacity = 25,
                    Accessible = true,
                    Kosher = false,
                    OpeningTime = DateTime.Today.Add(TimeSpan.Parse("12:00:00")),
                    ClosingTime = DateTime.Today.Add(TimeSpan.Parse("22:00:00")),
                    ImgUrl = "/Content/burger.jpg"
                }
            };

            if (!context.Restaurant.Any())
            {
                context.Restaurant.AddRange(restaurants);
                context.SaveChanges();
            }

            var users = context.Users.ToList();
            var orders = new List<Order>()
            {
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("user")),
                    Restaurant = restaurants[0],
                    DateTime = new DateTime(2021, 7, 8, 20, 00, 00),
                    Comments = "Extra sauce",
                    NumberOfSeats = 4
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("user")),
                    Restaurant = restaurants[1],
                    DateTime = new DateTime(2021, 7, 11, 10, 00, 00),
                    Comments = "Celebrating a friend's birthday",
                    NumberOfSeats = 7
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("user")),
                    Restaurant = restaurants[2],
                    DateTime = new DateTime(2021, 7, 12, 21, 00, 00),
                    Comments = "We want it faster",
                    NumberOfSeats = 5
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("itay")),
                    Restaurant = restaurants[3],
                    DateTime = new DateTime(2021, 7, 13, 17, 00, 00),
                    Comments = "Need forks please",
                    NumberOfSeats = 2
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("itay")),
                    Restaurant = restaurants[4],
                    DateTime = new DateTime(2021, 7, 14, 14, 00, 00),
                    Comments = "Nothing special, thanks",
                    NumberOfSeats = 10
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("linoy")),
                    Restaurant = restaurants[5],
                    DateTime = new DateTime(2021, 7, 15, 14, 30, 00),
                    Comments = "We want to sit outside",
                    NumberOfSeats = 4
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("linoy")),
                    Restaurant = restaurants[0],
                    DateTime = new DateTime(2021, 7, 18, 15, 00, 00),
                    Comments = "",
                    NumberOfSeats = 3
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("linoy")),
                    Restaurant = restaurants[1],
                    DateTime = new DateTime(2021, 7, 19, 9, 00, 00),
                    Comments = "",
                    NumberOfSeats = 5
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("amir")),
                    Restaurant = restaurants[2],
                    DateTime = new DateTime(2021, 7, 20, 19, 00, 00),
                    Comments = "Extra sauce",
                    NumberOfSeats = 8
                },
                new()
                {
                    Customer = users.First(u => u.UserName.Equals("amir")),
                    Restaurant = restaurants[3],
                    DateTime = new DateTime(2021, 7, 21, 13, 30, 00),
                    Comments = "We want to sit inside",
                    NumberOfSeats = 2
                }
            };

            if (!context.Order.Any())
            {
                context.Order.AddRange(orders);
                context.SaveChanges();
            }
        }
    }
}
